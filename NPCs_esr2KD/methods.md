#Notes

No outliers, clear clustering. 
Performed with and without Day 0 since originally we only discussed the Day6 DEX genes in manuscript. However, the day 0 counts help add significance and context for some genes that are lowly expressed in Day 6 (e.g. EMX2)



#Methods
Libraries were generated with the Ion Ampliseq Transcriptome Human Gene Expression Chef-Ready Kit (Life Technologies) according to the recommended protocol. The resulting libraries were templated onto Ion 540 Chips using the Ion Chef system and sequenced on the Ion GeneStudio S5 System. Raw data was processed with the Ion Torrent Suite Server v5.8.0 (ThermoFisher). Reads were aligned reads to the hg19 AmpliSeq Transcriptome reference (v1.1) with the coverageAnalysis plugin v5.8.0.8. This plugin also generated quality information. Reads were quantified using the ampliSeqRNA plugin v5.8.0.3 with default settings. 

Genes with greater than 1 count per million in three samples were retained for clustering and DEX analysis. No outlier samples were determined. Genes were tested for differential expression using DESeq2 v1.24.0 (Love et al., 2014) with shrinkage estimator apeglm (Zhu et al., 2018). Significantly DEX genes are genes that pass a 0.05 significant threshold. 

Gene Ontology enrichment analysis of DEX genes was performed using the R package goseq (v 1.34.1) using all expressed genes (N=13254) as background.
