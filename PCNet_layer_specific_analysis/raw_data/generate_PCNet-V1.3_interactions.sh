## data
# downloaded from: https://www.ndexbio.org/#/network/4de852d9-9908-11e9-bcaf-0ac135e8bacf
# Updated PCNet v1.3 
# Nodes: 18820	Edges: 2693250
# Owner	Tongqiu Jia
# Created	Jun 27, 2019 1:20:55 PM
# Last Modified	Jun 27, 2019 1:33:42 PM
# UUID	4de852d9-9908-11e9-bcaf-0ac135e8bacf
# Description:
# Node name update of Parsimonious Composite Network (PCNet, uuid: f93f402c-86d4-11e7-a10d-0ac135e8bacf) .

# Node names (gene symbols) are updated using HGNC Multi-symbol checker (see reference). Previous symbols, synonyms are updated to approved symbols. Nodes with unmatched symbols are dropped from the network.

# Updated Jun 27, 2019
# Version: 1.3

## after downloading, data was opened in Cytoscape v3.8.1, then Edge Table was exported to 'Updated_PCNetv1.3_default_edge.csv'

## navigate to data directory
cd ~/Google\ Drive\ File\ Stream/Shared\ drives/Phase\ 2\ ASD\ Network\ Analysis/data/PCNet\ Molecular\ Interaction\ Data/raw_data 

## first look at what file looks like
head Updated_PCNetv1.3_default_edge.csv 
# "interaction","name","selected","shared interaction","shared name"
# "neighbor-of","UBE2Q1 (neighbor-of) RNF14","false","neighbor-of","UBE2Q1 (neighbor-of) RNF14"
# "neighbor-of","UBE2Q1 (neighbor-of) UBE2Q2","true","neighbor-of","UBE2Q1 (neighbor-of) UBE2Q2"
# "neighbor-of","UBE2Q1 (neighbor-of) TMCO1","false","neighbor-of","UBE2Q1 (neighbor-of) TMCO1"
# "neighbor-of","UBE2Q1 (neighbor-of) UBAC1","false","neighbor-of","UBE2Q1 (neighbor-of) UBAC1"
# "neighbor-of","UBE2Q1 (neighbor-of) RAB4A","false","neighbor-of","UBE2Q1 (neighbor-of) RAB4A"
# "neighbor-of","UBE2Q1 (neighbor-of) ZNF706","false","neighbor-of","UBE2Q1 (neighbor-of) ZNF706"
# "neighbor-of","UBE2Q1 (neighbor-of) MIB2","false","neighbor-of","UBE2Q1 (neighbor-of) MIB2"
# "neighbor-of","UBE2Q1 (neighbor-of) RNF114","false","neighbor-of","UBE2Q1 (neighbor-of) RNF114"
# "neighbor-of","UBE2Q1 (neighbor-of) RNF115","false","neighbor-of","UBE2Q1 (neighbor-of) RNF115"

### need to remove 'neighbor-of' to get to interactions
### use comma as initial delimiter to chunk columns

## next check we can grab field/column 2 correctly using comma
awk -F ',' '{print $2}' Updated_PCNetv1.3_default_edge.csv | head
# "name"
# "UBE2Q1 (neighbor-of) RNF14"
# "UBE2Q1 (neighbor-of) UBE2Q2"
# "UBE2Q1 (neighbor-of) TMCO1"
# "UBE2Q1 (neighbor-of) UBAC1"
# "UBE2Q1 (neighbor-of) RAB4A"
# "UBE2Q1 (neighbor-of) ZNF706"
# "UBE2Q1 (neighbor-of) MIB2"
# "UBE2Q1 (neighbor-of) RNF114"
# "UBE2Q1 (neighbor-of) RNF115"

### looks good

## now need to split apart this field into the chunks we want
awk -F ',' '{print $2}' Updated_PCNetv1.3_default_edge.csv | awk -F ' ' '{print $1,$3}' | head
# "name" 
# "UBE2Q1 RNF14"
# "UBE2Q1 UBE2Q2"
# "UBE2Q1 TMCO1"
# "UBE2Q1 UBAC1"
# "UBE2Q1 RAB4A"
# "UBE2Q1 ZNF706"
# "UBE2Q1 MIB2"
# "UBE2Q1 RNF114"
# "UBE2Q1 RNF115"

awk -F ',' '{print $2}' Updated_PCNetv1.3_default_edge.csv | awk -F ' ' '{print $1,$3}' | awk '{gsub("\"", ""); print}' | tail -n+2 > "../PCNet-V1.3_interactions.txt"

# check output
head PCNet-V1.3_interactions.txt
# UBE2Q1 RNF14
# UBE2Q1 UBE2Q2
# UBE2Q1 TMCO1
# UBE2Q1 UBAC1
# UBE2Q1 RAB4A
# UBE2Q1 ZNF706
# UBE2Q1 MIB2
# UBE2Q1 RNF114
# UBE2Q1 RNF115
# UBE2Q1 RNF111
### looks good

## check number of lines, should be 2,693,250
wc -l PCNet-V1.3_interactions.txt
#  2693250 PCNet-V1.3_interactions.txt

