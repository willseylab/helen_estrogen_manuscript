library(tidyverse)
library(parallel)

wd <- "/Users/adapt/Bitbucket/helen_estrogen_manuscript/PCNet_layer_specific_analysis"
dd <- "/Users/adapt/Bitbucket/helen_estrogen_manuscript/PCNet_layer_specific_analysis/data"
od <- "/Users/adapt/Bitbucket/helen_estrogen_manuscript/PCNet_layer_specific_analysis/results"
pd <- "/Users/adapt/Bitbucket/helen_estrogen_manuscript/PCNet_layer_specific_analysis/permutations"

# wd <- "/home/rstudio"
# sd <- "/home/rstudio/scripts"
# dd <- "/home/rstudio/data"
# od <- "/home/rstudio/results"
# pd <- "/home/rstudio/permutations"

setwd(wd)

### part 1 --- parse PCNET data

# load pcnet data
pcnet <- read_table2(file.path(dd, "PCNet-V1.3_interactions.txt"), col_names = c("gene1", "gene2"))
# remove instances of self interactions
pcnet <- filter(pcnet, gene1 != gene2)
# confirm all interactions are unique
dim(pcnet); dim(distinct(pcnet))

# load permuted hcASD risk genes
( permuted_hcASD <- load(file.path(pd, "1000_permuted_hcASD-gene-sets.RData")) )

# create a function to trim PCNet interactions to those involving an hcASD gene
## also adds back in interactions between any of these genes
### note this is based on "parse_PCnet.R"
trim_pcnet <- function(hcASDgenes, pcnet = pcnet){
  
  # trim to subset of interactions involving an hcASD gene
  pcnet_asd <- pcnet %>%
    filter(gene1 %in% hcASDgenes | gene2 %in% hcASDgenes)
  
  # create list of all genes in pcnet
  genes_in_all_pcnet <- unique(c(pcnet$gene1, pcnet$gene2))
  
  # create list of unique genes
  genes_in_pcnet_asd <- unique(c(pcnet_asd$gene1, pcnet_asd$gene2))
  
  # add back in interactions between any of these genes, i.e. right now
  # only interactions between hcASD genes and other genes are included
  pcnet_asd_completeInteractions <- pcnet %>%
    filter((gene1 %in% pcnet_asd$gene1 | gene1 %in% pcnet_asd$gene2) & (gene2 %in% pcnet_asd$gene1 | gene2 %in% pcnet_asd$gene2))
  
  # remove instances of self interactions
  pcnet_asd <- filter(pcnet_asd, gene1 != gene2)
  pcnet_asd_completeInteractions <- filter(pcnet_asd_completeInteractions, gene1 != gene2)
  
  return(list(genes_in_all_pcnet = genes_in_all_pcnet,
              pcnet_asd = pcnet_asd, genes_in_pcnet_asd = genes_in_pcnet_asd,
              pcnet_asd_completeInteractions = pcnet_asd_completeInteractions))
}

### part 2 --- parse layer specific data

# load brainspan layer specific data from Miller Nature 2014
(BrainSpan_FinalData_9_6_12 <- load(file.path(dd, "BrainSpan_FinalData_9_6_12.RData")))

# create functions to parse layer specific data
## trimming to genes part of PCNet
## connected to an hcASD gene
## and expressed in a given layer
 
## for a given sample index (e.g. 1-4)
## this function gets data from a particular region and layer of the brain then
## trims to probes detected above background and then
## summarizes at the gene level by taking the median of all probes for each gene
### note this is based on "parse_layer-specific-data.R"
by_layer <- function(sampleIndex, layerList, generateBG, geneList, bgGeneList){
  ## first get all instances of structure that match
  ### gene expression data (ComBAT) is of the form: rows = genes, columns = structure
  structureIndex <- str_which(info[[sampleIndex]]$Structure, layerList[[1]])
  ## now trim to gene expression data for these samples/structures only
  ### also add probes and gene names to data
  ### use select in order to retain df structure in instances where there is
  ### only one matching sample otherwise you get a vector without a name that
  ### throws an error
  expression <- info[[sampleIndex]]$ComBat %>%
    as_tibble(.name_repair = "unique") %>%
    select(all_of(structureIndex)) %>%  
    mutate(gene = genes)
  ## now trim this further to get probes with expression detected above background (DABG) (i.e. where PMA = 1)
  ### make data frame for PMA that is equivalent to expression data
  pmaDF <- info[[sampleIndex]]$PMA %>%
    as_tibble(.name_repair = "unique") %>%
    select(all_of(structureIndex))
  ### use this data frame to filter expression
  #### here rowSums ncol - 1 means that probe is DABG for all samples ** we may want to modify this
  #### subtract -1 from ncol to account for column with gene names
  expression <- filter(expression, rowSums(pmaDF) == (ncol(expression)-1))
  # collapse to gene level data
  trimmed_expression <- expression %>%
    group_by(gene) %>%
    summarise(across(!matches("gene"), median))
  # trim to genes in PCNet_ASD (unless generating background)
  if(generateBG == "no"){
    trimmed_expression %>% filter(gene %in% geneList)
  } else{
    trimmed_expression %>% filter(gene %in% bgGeneList)
  }
}

# create wrapper function to get data from every layer,
## combining data across samples to get one data frame per layer
### note this is based on "parse_layer-specific-data.R"
get_all_data <- function(layerIndex, sampleIndex, geneList, bgGeneList, generateBG){
  layerData_allSamples <- lapply(sampleIndex, function(x) by_layer(x, layerIndex, generateBG, geneList, bgGeneList))
  layerData_allSamples_combined <- layerData_allSamples %>% reduce(inner_join)
}

# create a new function to create byLayer data for each permutation
get_permutation_byLayer_data <- function(permutedPCNet, filtering_list){
  # get hcASD gene specific data for all layers being targeted, but from just PCW 15 and 16 donors
  # note that background gene list is the same across all permutations, by layer
  ## this bg list is just all PCNET interactions present in a given layer
  byLayer_Data_permutation <- lapply(filtering_list, 
                                       function(x) get_all_data(layerIndex = x, sampleIndex = 3:4, 
                                                                geneList = permutedPCNet$genes_in_pcnet_asd,
                                                                bgGeneList = permutedPCNet$genes_in_all_pcnet,
                                                                generateBG = "no")
                                     )
}

### part 3 --- assess connectivity by layer

# load layer specific data, summarized by layer, already cleaned
## will only use background data
(allExpressionData <- load(file.path(dd, "BrainSpan-Data_byLayer_cleaned.RData")))
##so just in case delete rest of data loaded--> it's not needed
rm(list = allExpressionData[which(allExpressionData != "byLayer_bgData_PCW15_16_only")])

# functions

calculateCor <- function(layer, expressionData, corMethod = "spearman"){
  layerCor <- expressionData[[layer]] %>% 
    select(-gene) %>%
    as.matrix() %>% 
    t %>% 
    cor(method = corMethod)
  colnames(layerCor) <- expressionData[[layer]]$gene
  rownames(layerCor) <- expressionData[[layer]]$gene
  return(layerCor)
}

getNormalizedConnectivity <- function(layer, permutedGeneSet, pcnet_permutedGenes, allCors, averageCor, trueInteractions){
  # extract correlation matrix for this layer
  layerCor <- allCors[[layer]]
  
  # capture correlations that correspond to PCnet interactions
  layerSpecific_trueInteractions <- trueInteractions %>% filter(gene1 %in% colnames(layerCor) & gene2 %in% colnames(layerCor))
  
  # get correlations for PCNET interactions in this layer
  interactionCorrelations <- layerCor[as.matrix(layerSpecific_trueInteractions)]
  
  # get number of interactions
  numInteractions <- nrow(layerSpecific_trueInteractions)
  
  # get number of genes
  numberGenes <- ncol(layerCor)
  
  # calculate normalized connectivity
  layerK <- sum(abs(interactionCorrelations))
  ## extract average correlation in this layer
  backgroundAveCor <- averageCor[[layer]]
  ## normalize
  normedLayerK <- layerK / backgroundAveCor
  
  normedK_perGene <- normedLayerK / numberGenes
  normedK_perInteraction <- normedLayerK / numInteractions
  
  # also get connectivity of hcASD genes only
  ## this works because pcnet_asd and layerSpecific_trueInteractions are subsets of the same list, with same gene1 and gene2 structure
  hcASD_interactions <- semi_join(x = layerSpecific_trueInteractions, y = pcnet_permutedGenes, by=c("gene1","gene2"))
  numInteractions_hcASD <- nrow(hcASD_interactions)
  # number_hcASDgenes <- filter(permutedGeneSet, (gene %in% hcASD_interactions$gene1 | hugoGene %in% hcASD_interactions$gene1) |
  #                               (gene %in% hcASD_interactions$gene2 | hugoGene %in% hcASD_interactions$gene2)) %>% nrow
  # 
  number_hcASDgenes <- which(permutedGeneSet %in% hcASD_interactions$gene1 | permutedGeneSet %in% hcASD_interactions$gene2) %>%
    length()
  
  # get correlations for hcASD interacions in this layer
  hcASD_interactionCorrelations <- layerCor[as.matrix(hcASD_interactions)]
  # get connectivity
  hcASD_k <- sum(abs(hcASD_interactionCorrelations))
  normed_hcASD_k <- hcASD_k / backgroundAveCor
  normed_hcASD_k_perGene <- normed_hcASD_k / number_hcASDgenes
  normed_hcASD_k_perInteraction <- normed_hcASD_k / numInteractions_hcASD
  
  return(list(numberGenes = numberGenes, numInteractions = numInteractions, 
              layerK = layerK, backgroundAveCor = backgroundAveCor, normedLayerK = normedLayerK,
              normedK_perGene = normedK_perGene, normedK_perInteraction = normedK_perInteraction,
              number_hcASDgenes = number_hcASDgenes, numInteractions_hcASD = numInteractions_hcASD,
              normed_hcASD_k = normed_hcASD_k, normed_hcASD_k_perGene = normed_hcASD_k_perGene,
              normed_hcASD_k_perInteraction = normed_hcASD_k_perInteraction))
}

# create wrapper function for connectivity analysis
get_all_connectivity <- function(permutedGeneSet, layers, bgData = byLayer_bgData_PCW15_16_only, 
                                 layerData, pcnet_permutedGenes, trueInteractions){
  # get background correlations to use later
  bgMetrics_permutation <- lapply(layers, function(x){
    tmp = calculateCor(x, bgData)
    sumLayerCor = tmp[upper.tri(tmp, diag = F)] %>% abs %>% sum
    averageLayerCor = tmp[upper.tri(tmp, diag = F)] %>% abs %>% mean
    numGenes = nrow(tmp)
    return(list(numGenes = numGenes, sumLayerCor = sumLayerCor, averageLayerCor = averageLayerCor))
  }
  )
  
  # this will be used later on in indexed by layer name
  names(bgMetrics_permutation) <- layers
  averageLayerCor_permutation <- lapply(bgMetrics_permutation, function(x) x$averageLayerCor)
  #this is used for summary
  (bgMetrics_permutation <- bind_rows(bgMetrics_permutation) %>% mutate(layer = layers))
  
  # get layer correlations up front to avoid calculating multiple times
  layerCors_permutation <- lapply(layers, function(x) calculateCor(x, layerData))
  names(layerCors_permutation) <- layers
  
  # calculate normalized connectivity by layer
  connectivityByLayer_permutation <- lapply(layers, function(x) getNormalizedConnectivity(layer = x, permutedGeneSet, pcnet_permutedGenes,
                                                                                            allCors = layerCors_permutation, 
                                                                                            averageCor = averageLayerCor_permutation, 
                                                                                            trueInteractions = trueInteractions))
  # names(connectivityByLayer) <- layers
  (connectivityByLayer_permutation <- bind_rows(connectivityByLayer_permutation) %>%
      mutate(layer = layers) %>%
      select(layer, numberGenes:normed_hcASD_k_perInteraction)
  )
  
}

# create wrapper for permutation test

do_permutation_test <- function(permutedGeneSet){
  tmp1 = trim_pcnet(permutedGeneSet, pcnet = pcnet)
  tmp2 = get_permutation_byLayer_data(tmp1, filtering_list)
  tmp3 = get_all_connectivity(permutedGeneSet, layers, bgData = byLayer_bgData_PCW15_16_only, 
                              layerData = tmp2, pcnet_permutedGenes = tmp1$pcnet_asd, trueInteractions = tmp1$pcnet_asd_completeInteractions)
}


##### actual permutation test

filtering_list <- list(CPo = "^fCP[a-z0-9]*o$", CPi = "^fCP[a-z0-9]*i$", 
                       SP = "^fSP[a-z0-9]*$", IZ = "^fIZ[a-z0-9]*$", 
                       SZo = "^fSZ[a-z0-9]*o$", SZi = "^fSZ[a-z0-9]*i$", 
                       VZ = "^fVZ[a-z0-9]*$"
                       )
layers <- names(filtering_list)


# permutations <- mclapply(permuted_hcASD_genes, function(x) do_permutation_test(x),
#                          mc.cores = (detectCores()-1) 
#                          )

# test with known hcASD genes
load(file = file.path(dd, "hcASD.RData"))
knownResult <- do_permutation_test(hcASD$hugoGene)



### code to parse permutations

all_permutations_table <- bind_rows(permutations)
save(all_permutations_table,
     file = file.path(pd, "permutationTest_random-hcASD-genes-FDR-LE-01_1000-iterations.RData"))

CPo_permutations <- filter(all_permutations_table, layer == "CPo")
which(CPo_permutations$normed_hcASD_k > 36246.69) %>% length / 1000

CPi_permutations <- filter(all_permutations_table, layer == "CPi")
which(CPi_permutations$normed_hcASD_k > 37645.51) %>% length / 1000

SP_permutations <- filter(all_permutations_table, layer == "SP")
which(SP_permutations$normed_hcASD_k > 38509.43) %>% length / 1000

IZ_permutations <- filter(all_permutations_table, layer == "IZ")
which(IZ_permutations$normed_hcASD_k > 37963.28) %>% length / 1000

SZo_permutations <- filter(all_permutations_table, layer == "SZo")
which(SZo_permutations$normed_hcASD_k > 36383.43) %>% length / 1000

SZi_permutations <- filter(all_permutations_table, layer == "SZi")
which(SZi_permutations$normedLayerK > 1598417) %>% length / 1000
which(SZi_permutations$normedK_perGene > 206.5672) %>% length / 1000
which(SZi_permutations$normedK_perInteraction > 1.182550) %>% length / 1000
which(SZi_permutations$normed_hcASD_k > 37830.04) %>% length / 1000
which(SZi_permutations$normed_hcASD_k_perGene > 467.0376) %>% length / 1000
which(SZi_permutations$normed_hcASD_k_perInteraction > 1.129727) %>% length / 1000
which(SZi_permutations$number_hcASDgenes > 81) %>% length / 1000

VZ_permutations <- filter(all_permutations_table, layer == "VZ")
which(VZ_permutations$normedLayerK > 1572804) %>% length / 1000
which(VZ_permutations$normedK_perGene > 197.1180) %>% length / 1000
which(VZ_permutations$normedK_perInteraction > 1.118493) %>% length / 1000
which(VZ_permutations$normed_hcASD_k > 37733.57) %>% length / 1000



