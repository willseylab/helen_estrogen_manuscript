# README #

This repository contains the analysis accompanying 'Autism risk genes and estrogen signaling converge to regulate forebrain neurogenesis' H.Willsey et al. 

All folders have same structure:	

/scripts -- final scripts used in pub	
/results 	
/raw_data -- must be raw data -- processed data is in results	
methods.md -- methods for that specific analysis. 	
/ any other directories are to preserve older analyses and are not necessarily used in final material 	

### NPCs_esr2KD ###

* NPC cells +/- ESR2 KD at Day0 and Day 6
* Supplemental Figure of VIM/EMX2/HES1
* Table S9 and S10
* Did analysis both with and without Day 0 since the original thought was to only report Day6; however we find the context of Day 0 is important so the publication will only be the "withDay0" results

### xentrop_cyclo_best ###

* Xenopus trop Stage 46 brain RNAseq following CYCLO/bEST/DMSO treatment
* Figure 5AB
* Table S4, S6, S7

### human_neuron_glia_Fried2009 ###

* Comparison of Xenopus bEST DEX genes from above to human GW18 neuron culture
* Only interested in common disreg genes in humans across neuron/neuron +glia so we collapse them. The resulting list is {estrogen neuron, estrogen neuron+glia} vs {neuron, neuron+glia}
* FYI neuron-only culture seems to reflect Xenopus better so we're actually hindering ourselves/being conservative by including the neuron+glia
* Figure 6A
* Table S8
* Processing_DEX.Rmd is functional script adapted from Jeremy's work. The original scripts and the other accompanying images are preserved in 'JeremyPlots_Scripts' although they will need minor path edits to run again. 


### xentrop_timecourse ###
* RNAseq xentrop brain timecourse Stage 40-47
* Version 1 = CONTEXT -- proved not robust with the inclusion of many more genes post-realignment (scripts preserved in context_original_material)
* Version 2 = WGCNA -- difficult, too many parameters to optimize given time
* Version 3 = PCA +SEA
* Figure 2C,D

### publication_materials ###

* Execute Images_script.html to regenerate every Figure and Table associated with publication. Names may need changing depending on manuscript versions. 

### geo_submission ###

* Work in progress of what files/raw data needs to be submitted upon the publication being accepted.
* GEO submission sheet located within
* Most files still on aws, for the processed files execute script to regenerate them from within this repository. 


