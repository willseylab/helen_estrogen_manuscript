raw files in aws: tropicalis-asd/07-30-18_rnaseq/fastq

RNA-Seq reads were aligned to the XenBase X. tropicalis v9.1 reference genome using STAR v2.7.3 (Dobin et al., 2013) in gene annotation mode. RNA-seq quality control metrics were generated using Picard (v 2.21.1). Genes with greater than 1 counts per million in at least three samples were retained. No samples were determined an outlier by quality metric PCA or gene expression clustering described above. Full-quantile normalized counts per million for all X. tropicalis genes that have a human ortholog (N=) were provided to CoNTExT (Stein et al., 2014). Stage 40 samples were denoted the “progenitor” group; however, multiple iterations changing this parameter proved robust. 

Also submitted to context:
1) specifying stages 40-45 as progenitor
2) specifying stage 47 as progenitor
with no dramatic change

"described above" == Principal component analysis (PCA) of the quality control matrices did not indicate any sample outliers. To determine any sample outliers by gene expression, we preformed complete-linkage clustering based on the Euclidean distance of the Pearson correlation matrix. We defined an outlier as a sample which only shared the deepest node with replicates. 

